
(function init() {
  $(document).on('click', '#button-login', login);
  $(document).on('click', 'section[location="search"] .header', snackbar);
  $(document).on('click', '#button-nav', toggleNav);
  $(document).on('click', '#button-alert-create', createAlert);
  $(document).on('click', '#button-report-create', createReport);
  $(document).on('click', '#button-create-report', createReportfromAlert);
  $(document).on('click', '#alert-list .alert-item:not(.active)', function(){
    viewAlert($(this), false);
  });
  $(document).on('click', '#alert .alert-item', function() {
    viewAlert($(this), true);
  });
  $(document).on('click', '#navigation ul li', function(){
    gotoLocation($(this).attr('location'));
    if($(this).attr('location') == 'cameras') {
      camerasMap();
    }
    toggleNav();
  });
  $(document).on('keyup change', '#form-report-notes', autoresize);
  $(function() {
    populateAlertList();
  });

  $(document).on('keyup change focus', '#form-report-notes', autoresize);
  $(document).on('keyup change focus', '#detail-notes', autoresizeDetail);
  // register jQuery extension
  jQuery.extend(jQuery.expr[':'], {
      focusable: function (el, index, selector) {
          return $(el).is('a, button, :input, [tabindex]');
      }
  });

  $(document).on('keypress', 'input,select,textarea', function (e) {
      if (e.which == 13) {
          e.preventDefault();
          // Get all focusable elements on the page
          var $canfocus = $(':focusable');
          var index = $canfocus.index(this) + 1;
          if (index >= $canfocus.length) index = 0;
          $canfocus.eq(index).focus();
      }
  });

})();

function login() {
  $('#button-login').attr('disabled', true);
  $('#logo .logo-icon').velocity({rotateZ: '360deg'}, { loop: 2, duration: 500 });
  setTimeout(function(){
    $('section[location="login"]').velocity({top: '-100vh', opacity: '0'}, 500);
    $('#video').fadeOut();
    $('section[location="search"]').delay('100').fadeIn('slow');
    $('#navigation').fadeIn();
    $('#navigation ul li:not([location="search"])').removeClass('current');
    $('#navigation ul li[location="search"]').addClass('current');
  }, 1000);
}

var carData = [
  {id: '1', image: 'media/car1.jpg', plateImage: 'media/plate1.png', date: '7/7/2017 2:17pm', plate: '57NA', lat: '51.501195', lng: '-0.128488', reader: 'LEO Test SET', notes: '', user: '', camera: '100_001_0001 Leo Camera Test', domain: 'LEO API Test', make: 'Audi', model: 'A8', color: 'White', type: 'Full', tax: '39'},
  {id: '2', image: 'media/car2.jpg', plateImage: 'media/plate2.png', date: '7/7/2017 2:17pm', plate: 'NU12WPN', lat: '51.501195', lng: '-0.128488', reader: 'LEO Test SET', notes: '', user: '', camera: '100_001_0001 Leo Camera Test', domain: 'LEO API Test', make: 'Vauxhall', model: 'Astra', color: 'White', type: 'Full', tax: '48'},
  {id: '3', image: 'media/car3.jpg', plateImage: 'media/plate3.png', date: '7/7/2017 2:17pm', plate: 'YD08UTN', lat: '51.501195', lng: '-0.128488', reader: 'LEO Test SET', notes: '', user: '', camera: '100_001_0001 Leo Camera Test', domain: 'LEO API Test', make: 'Volkswagen', model: 'Transporter T28 102 TDI SWB', color: 'Silver', type: 'Full', tax: '39'},
  {id: '4', image: 'media/car4.jpg', plateImage: 'media/plate4.png', date: '7/7/2017 2:17pm', plate: 'YG11VAE', lat: '51.501195', lng: '-0.128488', reader: 'LEO Test SET', notes: '', user: '', camera: '100_001_0001 Leo Camera Test', domain: 'LEO API Test', make: 'Volkswagen', model: 'Transporter T30 140 TDI LWB', color: 'Silver', type: 'Full', tax: '39'},
  {id: '5', image: 'media/car5.jpg', plateImage: 'media/plate5.png', date: '7/7/2017 2:17pm', plate: 'YY08FDK', lat: '51.501195', lng: '-0.128488', reader: 'LEO Test SET', notes: '', user: '', camera: '100_001_0001 Leo Camera Test', domain: 'LEO API Test', make: 'Nissan', model: 'Micra Acenta', color: 'Red', type: 'Full', tax: '48'}
];

// var addedCarData = [
//   {image: 'media/car6.jpg', date: '7/7/2017 2:17pm', plate: 'X476DBU', lat: '51.501195', lng: '-0.128488', reader: 'LEO Test SET', notes: '', user: '', camera: '100_001_0001 Leo Camera Test', domain: 'LEO API Test', make: 'Volvo', model: 'S40 1.9 XS', color: 'Green', type: 'Full', tax: '39'},
//   {image: 'media/car7.jpg', date: '7/7/2017 2:17pm', plate: 'EX02YDM', lat: '51.501195', lng: '-0.128488', reader: 'LEO Test SET', notes: '', user: '', camera: '100_001_0001 Leo Camera Test', domain: 'LEO API Test', make: 'Peugeot', model: '206 1.4 HDi LX', color: 'Blue', type: 'Full', tax: '39'},
//   {image: 'media/car8.jpg', date: '7/7/2017 2:17pm', plate: 'S713DUB ', lat: '51.501195', lng: '-0.128488', reader: 'LEO Test SET', notes: '', user: '', camera: '100_001_0001 Leo Camera Test', domain: 'LEO API Test', make: 'Volvo', model: 'V70 2.0 10v', color: 'Blue', type: 'Full', tax: '39'},
//   {image: 'media/car9.jpg', date: '7/7/2017 2:17pm', plate: 'AO51JTX', lat: '51.501195', lng: '-0.128488', reader: 'LEO Test SET', notes: '', user: '', camera: '100_001_0001 Leo Camera Test', domain: 'LEO API Test', make: 'Seat', model: 'Ibiza 1.4 Chill', color: 'Black', type: 'Full', tax: '39'},
//   {image: 'media/car10.jpg', date: '7/7/2017 2:17pm', plate: 'GJ55FHO', lat: '51.501195', lng: '-0.128488', reader: 'LEO Test SET', notes: '', user: '', camera: '100_001_0001 Leo Camera Test', domain: 'LEO API Test', make: 'Saab', model: '9-5 2.0 T Linear', color: 'Silver', type: 'Full', tax: '39'}
// ];

function randomCar() {
  var random = carData[Math.floor(Math.random()*carData.length)];
  return random;
}

// function randomAddedCar() {
//   var random = addedCarData[Math.floor(Math.random()*addedCarData.length)];
//   return random;
// }

function random() {
  return Math.floor(Math.random()*carData.length);
}

function populateAlertList() {
  var alert = $('#alert-list');
  for (var i = 0; i < carData.length; i++) {
    var car = carData[i];
    alert.append('<div class="alert-item" data-i="' + i + '"><div class="alert-image"></div><div class="alert-text"></div></div>');
    alert.find('.alert-image').last().css({'backgroundImage': 'url('+car.image+')'}).attr('car', car.image).attr('plate', car.plateImage);
    alert.find('.alert-text').last().html('<div><strong>'+car.plate+'</strong></div>'+'<div>'+car.make+', '+car.model+'</div>');
  }
  setInterval(function(){
    var addedCar = randomCar();
    if($('.alert-item.active').length === 0 && $('section[location="alerts"]:visible').length > 0) {
      // If not viewing an alert, and viewing alerts page
      $('<div class="alert-item" data-i="' + (carData.length) + '" style="height:0px;"><div class="alert-image" style="background-image:url('+addedCar.image+')" car="'+addedCar.image+'" plate="'+addedCar.plateImage+'"></div><div class="alert-text"><div><strong>'+addedCar.plate+'</strong></div>'+'<div>'+addedCar.make+', '+addedCar.model+'</div></div></div>')
        .prependTo(alert)
        .velocity({height: '100px'}, 300);
      carData.push(addedCar);
    }
  }, 20000);
}

function snackbar() {
  var index = random();
  var car = carData[index];
  var alert = $('#alert');
  alert.html('<div class="alert-item" data-i="' + index + '"><div class="alert-image"></div><div class="alert-text"></div></div>');
  alert.find('.alert-image').css({'backgroundImage': 'url('+car.image+')'}).attr('car', car.image).attr('plate', car.plateImage);
  alert.find('.alert-text').html('<div><strong>'+car.plate+'</strong></div>'+'<div>'+car.make+', '+car.model+'</div>');
  alert.velocity({bottom: '0px'}, 150);
  setTimeout(function() {
    if(alert.find('.alert-item.active').length === 0) {
      alert.velocity({bottom: '-120px'}, 150);
    }
  }, 5000);
}

var modalOpen = false;
var changeImage;

function viewAlert(alert, fromSnackbar) {
  modalOpen = true;
  var alertImage = $(alert).addClass('active').find('.alert-image');
  if(fromSnackbar) {
    alertImage.velocity({'width': '100vw', 'height': '300px', 'translateY': '-' + (120 + alertImage.offset().top) + 'px'}, 250);
    $('#alert').velocity({'bottom': '-120px'}, 250);
  } else {
    alertImage.velocity({'width': '100vw', 'height': '300px', 'translateY': '-' + alertImage.offset().top + 'px'}, 250);
  }
  var imageurl = 'plate';
  changeImage = setInterval(function(){
    switch (imageurl) {
      case 'plate':
        alertImage.css({'backgroundImage': 'url('+alertImage.attr(imageurl)+')'});
        imageurl = 'location';
        break;
      case 'location':
        alertImage.css({'backgroundImage': 'url(media/location.png)'});
        imageurl = 'car';
        break;
      case 'car':
        alertImage.css({'backgroundImage': 'url('+alertImage.attr(imageurl)+')'});
        imageurl = 'plate';
        break;
    }
  }, 3000);

  var car = carData[alert.attr('data-i')];
  var alertDetail = $('section[location="alert-detail"]');
  alertDetail.find('#detail-reg').val(car.plate);
  alertDetail.find('#detail-date').val(car.date);
  alertDetail.find('#detail-make').val(car.make);
  alertDetail.find('#detail-model').val(car.model);
  alertDetail.find('#detail-colour').val(car.color);
  alertDetail.find('#detail-lat').val(car.lat);
  alertDetail.find('#detail-lng').val(car.lng);
  alertDetail.find('#detail-type').val(car.type);
  alertDetail.find('#detail-tax').val(car.tax);
  alertDetail.find('#detail-notes').val('');
  alertDetail.fadeIn();
  alertDetail.scrollTop(0);

  toggleNav();
}

var navOpen = false;
function toggleNav() {
  if(modalOpen === false) {
    if (navOpen === false) {
      $('#nav-mask').velocity({'scale': '40'}, 250);
      $('#button-nav').velocity({'rotateZ': '45deg'}, 250);
      $('#navigation ul').velocity({'left': '50px', 'opacity': '1'}, 500);
      navOpen = true;
    } else {
      $('#nav-mask').velocity({'scale': '1'}, 250);
      $('#button-nav').velocity({'rotateZ': '0deg'}, 250);
      $('#navigation ul').velocity({'left': '-200px', 'opacity': '0'}, 250);
      navOpen = false;
    }
  } else {
    if (navOpen === false) {
      $('#button-nav').velocity({'rotateZ': '45deg'}, 250);
      $('section[location="alerts"]').addClass('hideOverflow');
      navOpen = true;
    } else {
      $('#button-nav').velocity({'rotateZ': '0deg'}, 250);
      $('#alert-list').css('height', 'auto');
      var isSnackbar = $('.alert-item.active').parents('#alert').length > 0;
      clearInterval(changeImage);
      $('.alert-item.active .alert-image').css({'backgroundImage': 'url('+$('.alert-item.active .alert-image').attr('car')+')'});
      if(isSnackbar) {
        $('.alert-item.active').find('.alert-image').velocity({'translateY': '-1100px', 'opacity': 0}, 250);
        setTimeout(function() {
          $('.alert-item.active').removeClass('active').find('alert-image').css({opacity: 1, transform: 'translateY(0px)', width: '80px', height: '80px'});
        });
      } else {
        $('.alert-item.active').removeClass('active').find('.alert-image').velocity({'width': '100px', 'height': '100px', 'translateY': '0px'}, 250);
      }
      $('section[location="alerts"]').removeClass('hideOverflow');
      $('section[location="alert-detail"]').fadeOut();
      navOpen = false;
      modalOpen = false;
    }
  }
}

function gotoLocation(target) {
  if (target === 'login') {
    var current = $('#navigation ul li.current').attr('location');
    $('#button-login').attr('disabled', false);
    $('section[location="login"]').velocity({top: '0px', opacity: '1'}, 500);
    $('#video').fadeIn();
    $('section[location="'+current+'"]').delay('100').fadeOut('slow');
    $('#navigation').fadeOut();
  }
  $('section:not([location="'+target+'"])').hide();
  $('section[location="'+target+'"]').show();
  $('#navigation ul li').removeClass('current');
  $('#navigation ul li[location="'+target+'"]').addClass('current');
  $('section[location="'+target+'"]').scrollTop(0);
}

function autoresize() {
  var length = $('#form-report-notes').val().length;
  if (length < 30){
    $('#form-report-notes').velocity({'height': '40px'}, 150);
  } else if (length >= 30 && length < 60) {
    $('#form-report-notes').velocity({'height': '60px'}, 150);
  } else if (length >= 60 && length < 90) {
    $('#form-report-notes').velocity({'height': '80px'}, 150);
  } else if (length >= 90 && length < 120) {
    $('#form-report-notes').velocity({'height': '100px'}, 150);
  } else if (length >= 120) {
    $('#form-report-notes').val($('#form-report-notes').val().substring(0, 120));
  }
}

function autoresizeDetail() {
  var length = $('#detail-notes').val().length;
  if (length < 30){
    $('#detail-notes').velocity({'height': '40px'}, 150);
  } else if (length >= 30 && length < 60) {
    $('#detail-notes').velocity({'height': '60px'}, 150);
  } else if (length >= 60 && length < 90) {
    $('#detail-notes').velocity({'height': '80px'}, 150);
  } else if (length >= 90 && length < 120) {
    $('#detail-notes').velocity({'height': '100px'}, 150);
  } else if (length >= 120) {
    $('#detail-notes').val($('#detail-notes').val().substring(0, 120));
  }
}

var createAlertObj = [];
function createAlert() {
  createAlertObj.push(
    {
      start: $('#form-alert-start').val(),
      end: $('#form-alert-start').val(),
      make: $('#form-alert-make').val(),
      model: $('#form-alert-model').val(),
      color: $('#form-alert-color option:selected').val(),
      class: $('#form-alert-class option:selected').val(),
      plate: $('#form-alert-reg').val(),
      area: $('#form-alert-area option:selected').val()
    }
  );
  $('section[location="create-alert"]').find('input').val('');
  $('section[location="create-alert"]').find('select').val('');
  $('section[location="create-alert"]').animate({ scrollTop: 0 }, 'slow');
  $('.alert-success').html('Alert Created Successfully!').velocity({'bottom': '0px'}, 500).delay(3000).velocity({'bottom': '-100px'}, 500);
}

var createReportObj = [];
function createReport() {
  createReportObj.push(
    {
      make: $('#form-report-make').val(),
      model: $('#form-report-model').val(),
      color: $('#form-report-color option:selected').val(),
      class: $('#form-report-class option:selected').val(),
      plate: $('#form-report-reg').val(),
      notes: $('#form-report-notes').val()
    }
  );
  $('section[location="create-report"]').find('input').val('');
  $('section[location="create-report"]').find('select').val('');
  $('section[location="create-report"]').find('textarea').val('');
  $('section[location="create-report"]').animate({ scrollTop: 0 }, 'slow');
  $('.alert-success').html('Alarm Created Successfully!').velocity({'bottom': '0px'}, 500).delay(3000).velocity({'bottom': '-100px'}, 500);
}

function createReportfromAlert() {
  var alertDetail = $('section[location="alert-detail"]');
  $('#nav-mask').velocity({'scale': '40'}, 250).velocity({'scale': '1'}, 250);
  setTimeout(function(){
    toggleNav();
    gotoLocation('create-report');
  }, 250);
  $('section[location="create-report"]').find('#form-report-make').val( alertDetail.find('#detail-make').val() );
  $('section[location="create-report"]').find('#form-report-model').val( alertDetail.find('#detail-model').val() );
  $('section[location="create-report"]').find('#form-report-color').val( alertDetail.find('#detail-colour').val() );
  $('section[location="create-report"]').find('#form-report-reg').val( alertDetail.find('#detail-reg').val() );
  $('section[location="create-report"]').find('#form-report-notes').val( alertDetail.find('#detail-notes').val() );
}

var cameras = [
  {lat: 51.48867192318874, lng: -0.12947559356689453},
  {lat: 51.495104807610296, lng: -0.14198541641235352},
  {lat: 51.499005512399044, lng: -0.13048410415649414},
  {lat: 51.50418812420521, lng: -0.1289820671081543},
  {lat: 51.49661436652498, lng: -0.14616966247558594},
  {lat: 51.495438785128194, lng: -0.14571905136108398},
  {lat: 51.50040808149318, lng: -0.14061212539672852}
];

function camerasMap() {
  var center = {lat: 51.497482, lng: -0.135635};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 13,
    center: center,
    disableDefaultUI: true
  });

  function addMarker(pos, colour) {
    var marker = new google.maps.Marker({
      position: pos,
      map: map,
      icon: 'http://maps.google.com/mapfiles/ms/icons/'+colour+'-dot.png'
    });
  }
  // addMarker(center, 'green');

  for (var i = 0; i < cameras.length; i++) {
    addMarker(cameras[i], 'yellow');
  }

  google.maps.event.addListener(map, 'click', function(event) {
     console.log({lat: event.latLng.lat(), lng: event.latLng.lng()});
  });
}
